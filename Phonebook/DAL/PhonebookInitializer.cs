﻿using Phonebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Phonebook.DAL
{
    public class PhonebookInitializer : DropCreateDatabaseIfModelChanges<PhonebookContext>
    {
        protected override void Seed(PhonebookContext context)
        {
            Department department = new Department
            {
                Name = "Computer Center"
            };

            context.Departments.Add(department);

            Title programmer = new Title
            {
                Name = "Programmer"
            };

            context.Titles.Add(programmer);

            var personnels = new List<Personnel>
            {
                new Personnel { FirstName = "Luís", LastName = "Gonçalves", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+55 (12) 3923-5555" } }, Email = "luisg@embraer.com.br" },
                new Personnel { FirstName = "Leonie", LastName = "Köhler", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+49 0711 2842222" } }, Email = "leonekohler@surfeu.de" },
                new Personnel { FirstName = "François", LastName = "Tremblay", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (514) 721-4711" } }, Email = "ftremblay@gmail.com" },
                new Personnel { FirstName = "Bjørn", LastName = "Hansen", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+47 22 44 22 22" } }, Email = "bjorn.hansen@yahoo.no" },
                new Personnel { FirstName = "František", LastName = "Wichterlová", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+420 2 4172 5555" } }, Email = "frantisekw@jetbrains.com" },
                new Personnel { FirstName = "Helena", LastName = "Holý", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+420 2 4177 0449" } }, Email = "hholy@gmail.com" },
                new Personnel { FirstName = "Astrid", LastName = "Gruber", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+43 01 5134505" } }, Email = "astrid.gruber@apple.at" },
                new Personnel { FirstName = "Daan", LastName = "Peeters", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+32 02 219 03 03" } }, Email = "daan_peeters@apple.be" },
                new Personnel { FirstName = "Kara", LastName = "Nielsen", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+453 3331 9991" } }, Email = "kara.nielsen@jubii.dk" },
                new Personnel { FirstName = "Eduardo", LastName = "Martins", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+55 (11) 3033-5446" } }, Email = "eduardo@woodstock.com.br" },
                new Personnel { FirstName = "Alexandre", LastName = "Rocha", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+55 (11) 3055-3278" } }, Email = "alero@uol.com.br" },
                new Personnel { FirstName = "Roberto", LastName = "Almeida", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+55 (21) 2271-7000" } }, Email = "roberto.almeida@riotur.gov.br" },
                new Personnel { FirstName = "Fernanda", LastName = "Ramos", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+55 (61) 3363-5547" } }, Email = "fernadaramos4@uol.com.br" },
                new Personnel { FirstName = "Mark", LastName = "Philips", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (780) 434-4554" } }, Email = "mphilips12@shaw.ca" },
                new Personnel { FirstName = "Jennifer", LastName = "Peterson", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (604) 688-2255" } }, Email = "jenniferp@rogers.ca" },
                new Personnel { FirstName = "Frank", LastName = "Harris", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (650) 253-0000" } }, Email = "fharris@google.com" },
                new Personnel { FirstName = "Jack", LastName = "Smith", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (425) 882-8080" } }, Email = "jacksmith@microsoft.com" },
                new Personnel { FirstName = "Michelle", LastName = "Brooks", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (212) 221-3546" } }, Email = "michelleb@aol.com" },
                new Personnel { FirstName = "Tim", LastName = "Goyer", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (408) 996-1010" } }, Email = "tgoyer@apple.com" },
                new Personnel { FirstName = "Dan", LastName = "Miller", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (650) 644-3358" } }, Email = "dmiller@comcast.com" },
                new Personnel { FirstName = "Kathy", LastName = "Chase", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (775) 223-7665" } }, Email = "kachase@hotmail.com" },
                new Personnel { FirstName = "Heather", LastName = "Leacock", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (407) 999-7788" } }, Email = "hleacock@gmail.com" },
                new Personnel { FirstName = "John", LastName = "Gordon", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (617) 522-1333" } }, Email = "johngordon22@yahoo.com" },
                new Personnel { FirstName = "Frank", LastName = "Ralston", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (312) 332-3232" } }, Email = "fralston@gmail.com" },
                new Personnel { FirstName = "Victor", LastName = "Stevens", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (608) 257-0597" } }, Email = "vstevens@yahoo.com" },
                new Personnel { FirstName = "Richard", LastName = "Cunningham", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (817) 924-7272" } }, Email = "ricunningham@hotmail.com" },
                new Personnel { FirstName = "Patrick", LastName = "Gray", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (520) 622-4200" } }, Email = "patrick.gray@aol.com" },
                new Personnel { FirstName = "Julia", LastName = "Barnett", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (801) 531-7272" } }, Email = "jubarnett@gmail.com" },
                new Personnel { FirstName = "Robert", LastName = "Brown", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (416) 363-8888" } }, Email = "robbrown@shaw.ca" },
                new Personnel { FirstName = "Edward", LastName = "Francis", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (613) 234-3322" } }, Email = "edfrancis@yachoo.ca" },
                new Personnel { FirstName = "Martha", LastName = "Silk", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (902) 450-0450" } }, Email = "marthasilk@gmail.com" },
                new Personnel { FirstName = "Aaron", LastName = "Mitchell", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (204) 452-6452" } }, Email = "aaronmitchell@yahoo.ca" },
                new Personnel { FirstName = "Ellie", LastName = "Sullivan", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+1 (867) 920-2233" } }, Email = "ellie.sullivan@shaw.ca" },
                new Personnel { FirstName = "João", LastName = "Fernandes", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+351 (213) 466-111" } }, Email = "jfernandes@yahoo.pt" },
                new Personnel { FirstName = "Madalena", LastName = "Sampaio", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+351 (225) 022-448" } }, Email = "masampaio@sapo.pt" },
                new Personnel { FirstName = "Hannah", LastName = "Schneider", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+49 030 26550280" } }, Email = "hannah.schneider@yahoo.de" },
                new Personnel { FirstName = "Fynn", LastName = "Zimmermann", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+49 069 40598889" } }, Email = "fzimmermann@yahoo.de" },
                new Personnel { FirstName = "Niklas", LastName = "Schröder", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+49 030 2141444" } }, Email = "nschroder@surfeu.de" },
                new Personnel { FirstName = "Camille", LastName = "Bernard", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+33 01 49 70 65 65" } }, Email = "camille.bernard@yahoo.fr" },
                new Personnel { FirstName = "Dominique", LastName = "Lefebvre", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+33 01 47 42 71 71" } }, Email = "dominiquelefebvre@gmail.com" },
                new Personnel { FirstName = "Marc", LastName = "Dubois", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+33 04 78 30 30 30" } }, Email = "marc.dubois@hotmail.com" },
                new Personnel { FirstName = "Wyatt", LastName = "Girard", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+33 05 56 96 96 96" } }, Email = "wyatt.girard@yahoo.fr" },
                new Personnel { FirstName = "Isabelle", LastName = "Mercier", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+33 03 80 73 66 99" } }, Email = "isabelle_mercier@apple.fr" },
                new Personnel { FirstName = "Terhi", LastName = "Hämäläinen", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+358 09 870 2000" } }, Email = "terhi.hamalainen@apple.fi" },
                new Personnel { FirstName = "Hugh", LastName = "O'Reilly", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+353 01 6792424" } }, Email = "hughoreilly@apple.ie" },
                new Personnel { FirstName = "Lucas", LastName = "Mancini", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+39 06 39733434" } }, Email = "lucas.mancini@yahoo.it" },
                new Personnel { FirstName = "Johannes", LastName = "Van der Berg", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+31 020 6223130" } }, Email = "johavanderberg@yahoo.nl" },
                new Personnel { FirstName = "Stanisław", LastName = "Wójcik", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+48 22 828 37 39" } }, Email = "stanisław.wójcik@wp.pl" },
                new Personnel { FirstName = "Enrique", LastName = "Muñoz", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+34 914 454 454" } }, Email = "enrique_munoz@yahoo.es" },
                new Personnel { FirstName = "Joakim", LastName = "Johansson", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+46 08-651 52 52" } }, Email = "joakim.johansson@yahoo.se" },
                new Personnel { FirstName = "Emma", LastName = "Jones", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+44 020 7707 0707" } }, Email = "emma_jones@hotmail.com" },
                new Personnel { FirstName = "Phil", LastName = "Hughes", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+44 020 7976 5722" } }, Email = "phil.hughes@gmail.com" },
                new Personnel { FirstName = "Steve", LastName = "Murray", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+44 0131 315 3300" } }, Email = "steve.murray@yahoo.uk" },
                new Personnel { FirstName = "Mark", LastName = "Taylor", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+61 (02) 9332 3633" } }, Email = "mark.taylor@yahoo.au" },
                new Personnel { FirstName = "Diego", LastName = "Gutiérrez", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+54 (0)11 4311 4333" } }, Email = "diego.gutierrez@yahoo.ar" },
                new Personnel { FirstName = "Luis", LastName = "Rojas", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+56 (0)2 635 4444" } }, Email = "luisrojas@yahoo.cl" },
                new Personnel { FirstName = "Manoj", LastName = "Pareek", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+91 0124 39883988" } }, Email = "manoj.pareek@rediff.com" },
                new Personnel { FirstName = "Puja", LastName = "Srivastava", Title = programmer, Department = department, Phones = new List<Phone> { new Phone { Number = "+91 080 22289999" } }, Email = "puja_srivastava@yahoo.in" },
             };

            context.Personnels.AddRange(personnels);
        }
    }
}