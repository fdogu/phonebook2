﻿using Microsoft.AspNet.Identity.EntityFramework;
using Phonebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Phonebook.DAL
{
    public class PhonebookContext : IdentityDbContext<AppUser>
    {
        public PhonebookContext() : base("name=Phonebook")
        {

        }
        public DbSet<Personnel> Personnels { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Title> Titles { get; set; }
    }
}