﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Phonebook.Controllers
{
    public class PersonalDownloadController : Controller
    {
        private const string IsoFormat = "yyyy-MM-ddTHH:mm:ssZ";

        public ActionResult Index()
        {
            var videosFolder = Server.MapPath("~/App_Data/Videos");
            DirectoryInfo d = new DirectoryInfo(videosFolder);

            FileInfo[] files = d.GetFiles("*.*");

            var dateStr = DateTime.UtcNow.ToString(IsoFormat);

            var links = files
                .Select(f => f.Name + "\t" + dateStr + "\t" + "username" + "\t" + Guid.NewGuid())
                .Select(s => MachineKey.Protect(Encoding.Unicode.GetBytes(s)))
                .Select(b => HttpServerUtility.UrlTokenEncode(b))
                .ToList();

            return View(links);
        }

        public ActionResult Download(string link)
        {
            var b = HttpServerUtility.UrlTokenDecode(link);
            var s = Encoding.Unicode.GetString(MachineKey.Unprotect(b));
            var ss = s.Split('\t');
            var fileName = ss[0];
            var date = DateTime.ParseExact(ss[1], IsoFormat, CultureInfo.InvariantCulture);
            var userName = ss[2];

            if  (User.Identity.Name != userName)
            {
                return HttpNotFound();
            }
            if (DateTime.UtcNow - date > TimeSpan.FromHours(2))
            {
                return HttpNotFound();
            }
            Debug.WriteLine($"{fileName}, {DateTime.UtcNow - date}, {userName}");

            var fileNameOnly = Path.GetFileName(fileName);

            if (fileName != fileNameOnly)
                return HttpNotFound();

            var videosFolder = Server.MapPath("~/App_Data/Videos");

            Download(videosFolder, fileNameOnly, Response);

            return null;
        }

        static void Download(string folder, string fileName, HttpResponseBase response)
        {
            var filePath = Path.Combine(folder, fileName);

            response.Clear();
            response.ContentType = "application/octet-stream";
            response.AppendHeader("Content-Disposition", "filename=" + fileName);

            response.TransmitFile(filePath);

            response.End();
        }
    }
}