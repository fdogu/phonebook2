﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phonebook.Controllers
{
    public class DownloadController : Controller
    {
        public ActionResult Download(string fileName)
        {
            var fileNameOnly = Path.GetFileName(fileName);

            if (fileName != fileNameOnly)
                return HttpNotFound();

            var videosFolder = Server.MapPath("~/App_Data/Videos");

            var filePath = Path.Combine(videosFolder, fileNameOnly);

            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", "filename=" + fileNameOnly);

            Response.TransmitFile(filePath);

            Response.End();

            return null;
        }
    }
}