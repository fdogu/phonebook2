﻿using Phonebook.DAL;
using Phonebook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phonebook.Controllers
{
    public class PhotoController : Controller
    {
        private PhonebookContext db = new PhonebookContext();

        public ActionResult Download(int id)
        {
            var query = from p in db.Personnels
                            where p.Id == id
                            select new { p.Id, p.Photo };

            var personnel = query.Single();

            return File(
                fileContents: personnel.Photo,
                contentType: System.Net.Mime.MediaTypeNames.Image.Jpeg,
                fileDownloadName: $"{id}.jpg");
        }

        public ActionResult Upload()
        {
            var personnels = db.Personnels
                .Include(p => p.Title)
                .ToList();

            ViewData["Personnels"] = personnels;

            return View();
        }

        [HttpPost]
        public ActionResult Upload(int personnelId, HttpPostedFileBase file)
        {
            var personnel = db.Personnels.Find(personnelId);

            byte[] fileData;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }

            personnel.Photo = fileData;

            db.SaveChanges();

            return RedirectToAction("List", "Personnels");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}