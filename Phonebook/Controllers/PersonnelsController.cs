﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Phonebook.DAL;
using Phonebook.Models;
using Phonebook.ViewModels;

namespace Phonebook.Controllers
{
    public class PersonnelsController : Controller
    {
        private PhonebookContext db = new PhonebookContext();

        // GET: Personnels
        public ActionResult Index()
        {
            return View(db.Personnels.ToList());
        }

        public ActionResult List(SearchPersonnel search)
        {
            IQueryable<Personnel> queryable = db.Personnels;
            queryable = string.IsNullOrWhiteSpace(search.FirstName)
                ? queryable
                : queryable.Where(p => p.FirstName.Contains(search.FirstName));
            string lastName = Temizle(search.LastName);
            queryable = string.IsNullOrWhiteSpace(search.LastName)
                ? queryable
                : queryable.Where(p => p.LastName.Contains(lastName));

            var model = queryable
                .Include(p => p.Phones)
                .Include(p => p.Department)
                .Include(p => p.Title)
                .ToList()
                .Select(p => new PersonnelViewModel
                {
                    Id = p.Id,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    Phones = string.Join(", ", p.Phones.Select(ph => ph.Number)),
                    Email = p.Email,
                    Title = p.Title?.Name,
                    Department = p.Department?.Name,
                    Photo = p.Photo == null ? null : Url.Action("Download", "Photo", new { id = p.Id })
                });

            ViewData[nameof(SearchPersonnel)] = search;
            return View(model);
        }

        public static string Temizle(string p)
        {
            return p?.Replace("Ş", "S")?.Replace("Ğ", "G");
        }

        // GET: Personnels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personnel personnel = db.Personnels.Find(id);
            if (personnel == null)
            {
                return HttpNotFound();
            }
            return View(personnel);
        }

        // GET: Personnels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Personnels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Email")] Personnel personnel)
        {
            if (ModelState.IsValid)
            {
                db.Personnels.Add(personnel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(personnel);
        }

        // GET: Personnels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personnel personnel = db.Personnels.Find(id);
            if (personnel == null)
            {
                return HttpNotFound();
            }
            return View(personnel);
        }

        // POST: Personnels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Email")] Personnel personnel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(personnel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(personnel);
        }

        // GET: Personnels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personnel personnel = db.Personnels.Find(id);
            if (personnel == null)
            {
                return HttpNotFound();
            }
            return View(personnel);
        }

        // POST: Personnels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Personnel personnel = db.Personnels.Find(id);
            db.Personnels.Remove(personnel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
