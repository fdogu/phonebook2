﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phonebook.Controllers
{
    public class SimpleServiceController : Controller
    {
        SimpleService.SimpleServiceClient client = new SimpleService.SimpleServiceClient();

        // GET: SimpleService
        public ActionResult Index()
        {
            var list = client.GetUsers();
            return View(list);
        }

        // GET: SimpleService/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SimpleService/Create
        [HttpPost]
        public ActionResult Create(string userName)
        {
            try
            {
                client.AddUser(userName);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
