﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phonebook.Models
{
    public class Personnel : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<Phone> Phones { get; set; }
        public string Email { get; set; }
        public Title Title { get; set; }
        public Department Department { get; set; }
        public byte[] Photo { get; set; }
    }
}