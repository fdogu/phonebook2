﻿namespace Phonebook.Models
{
    public class Phone : Entity
    {
        public string Number { get; set; }
    }
}