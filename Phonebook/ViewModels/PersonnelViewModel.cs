﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phonebook.ViewModels
{
    public class PersonnelViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phones { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Department { get; set; }

        public string Photo { get; set; }
    }
}