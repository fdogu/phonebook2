﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Phonebook.DAL;
using Phonebook.Models;
using Owin.Security.CAS;
using System;
using System.Threading.Tasks;
using Microsoft.Owin.Security;

namespace Phonebook.App_Start
{
    public partial class Startup
    {
        private void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(() => new PhonebookContext());
            app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            app.CreatePerOwinContext<RoleManager<AppRole>>((options, context) =>
                new RoleManager<AppRole>(
                    new RoleStore<AppRole>(context.Get<PhonebookContext>())));

            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });

            app.UseCasAuthentication(new CasAuthenticationOptions()
            {
                //AuthenticationType = "Net ID", // change "CAS" to "Net ID" on the button and in DB
                //Caption = "Net ID", // change "CAS" to "Net ID" in tool tip
                CasServerUrlBase = "http://cantologin.metu.edu.tr/cas",
                //CasServerUrlBase = "http://cas.example.com:9000",


                Provider = new CasAuthenticationProvider
                {
                    OnAuthenticated = this.OnAuthenticated
                }
            });

        }

        private Task OnAuthenticated(CasAuthenticatedContext arg)
        {
            throw new NotImplementedException();
        }
    }
}